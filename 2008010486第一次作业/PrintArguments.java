/**
这是类PrintArguments的注释。
*/
public class PrintArguments
{
  /**
  *这是主函数
  *首先对参数的个数进行判断
  *当参数个数少于4时，用System包中out类的println方法输出字符串“Need for four arguments“
  *当参数个数大于等于4时，用System包中out类的println方法输出前四个参数
  */
  public static void main(String args[])
  {
    if(args.length<4)
    {
      System.out.println("Need for four arguments");
    }
    else 
    {
      System.out.println(args[0]+" "+args[1]+" "+args[2]+" "+args[3]);
    }
  }
}
/*本程序所犯错误：
System 首字母小写
args.length写成args.length()
*/
