import java.awt.Graphics;
import java.applet.Applet;

public class HelloQinghuaApplet extends Applet
{
  public String[] str= new String[4];
  
  public void init()
  {
    str[0]=new String("Hello Qinghua University!");
    str[1]=new String("Hello Prof. Xu!");
    str[2]=new String("My name is Wang Hongwei");
    str[3]=new String("My ID is 2008010486");
  }
  
  public void paint(Graphics g)
  {
    g.drawString(str[0],20,50);
    g.drawString(str[1],20,70);
    g.drawString(str[2],20,90);
    g.drawString(str[3],20,110);
  }
}
/*
注意：
类前也有public or private!
数组的声明格式：public String[] str= new String[4];
drawstring第一个为横坐标，第二个为纵坐标。
*/