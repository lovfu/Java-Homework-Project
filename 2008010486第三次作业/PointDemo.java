class PointDemo
{
  public static void main(String args[])
  {
    int i;
    Point p[]=new Point[10];
    for(i=0;i<10;i++)
    {
      p[i]=new Point();
    }
    System.out.println("调用Point.counter:");
    System.out.println("Point.counter = "+Point.counter);
    System.out.println("调用生成的Point数组中下标为5的所有的成员方法：");
    Point t=p[5];
    System.out.println("调用setPoint:");
    t.setPoint(50.5,60.3);
    System.out.println("调用getPoint:");
    t.getPoint();
    System.out.println("调用changePoint:");
    t.changePoint(40.8,100.4);
  }
}
class Point
{
  private double x;
  private double y;
  static int counter;
  Point()
  {
    counter++;
  }
  Point getPoint()
  {
    System.out.println("Get: x = "+x+" y = "+y);
    return this;
  }
  Point setPoint(double i,double j)
  {
    x=i;
    y=j;
    System.out.println("Set: x = "+x+" y = "+y);
    return this;
  }
  Point changePoint(double i,double j)
  {
    System.out.println("Before changed: x = "+x+" y = "+y);
    x=i;
    y=j;
    System.out.println("After changed: x = "+x+" y = "+y);
    return this;
  }
}