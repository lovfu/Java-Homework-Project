import java.util.regex.*;
class CaculatorDemo
{
  public static void main(String args[])
  {
    int x,y;
    x=9;y=4;
    System.out.println("x = "+x);
    System.out.println("y = "+y);
    System.out.println("以下是对类Caculator的测试：");
    System.out.println("x ＋ y：调用Caculator.plus()方法:");
    Caculator.plus(x,y);
    System.out.println("x * y：调用Caculator.mult()方法:");
    Caculator.mult(x,y);
    System.out.println("x ! ：调用Caculator.fact()方法:");
    Caculator.fact(x);
    System.out.println("x / y：调用SubCaculator.div()方法:");
    SubCaculator.div(x,y);
    System.out.println("x % y：调用SubCaculator.mod()方法:");
    SubCaculator.mod(x,y); 
    System.out.println("| x |：调用SubCaculator.abs()方法:");
    SubCaculator.abs(x);    
  }
}

class Caculator
{
  static int fact(int x)
  {
    if(x<0) return -1;
    int i,t=1;
    for(i=1;i<=x;i++)
      t*=i;
    System.out.println(x+" ! = "+t);
    return t;
  }
  static int plus(int x,int y)
  {
    int t=x+y;
    System.out.println(x+" + "+y+" = "+t);
    return t;
  }
  static int mult(int x,int y)
  {
    int t=x*y;
    System.out.println(x+" * "+y+" = "+t);
    return t;
  }
}

class  SubCaculator extends  Caculator
{
  static int mod(int x,int y)
  {
    int t=x%y;
    System.out.println(x+" % "+y+" = "+t);
    return t;
  }
  static int div(int x,int y)
  {
    int t=x/y;
    System.out.println(x+" / "+y+" = "+t);
    return t;
  }
  static int abs(int x)
  {
    int t=x>=0 ? x:x*(-1);
    System.out.println("| "+x+" | = "+t);
    return t; 
  }
}
//args.length表示传入参数的个数
//parseInt(args[i]),把字符串转换为Int
//比较两个字符串是否相同，一定要用equals()方法，否则，得到的总是false。