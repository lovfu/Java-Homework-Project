class CalculatorImplDemo
{
  public static void main(String args[])
  {
    int x,y;
    x=9;y=4;
    System.out.println("x = "+x);
    System.out.println("y = "+y);
    CalculatorImpl calculatorImpl=new CalculatorImpl();
    System.out.println("以下是对类CaculatorImpl的测试：");
    System.out.println("x ＋ y：调用CalculatorImpl.plus()方法:");
    calculatorImpl.plus(x,y);
    System.out.println("x * y：调用CalculatorImpl.mult()方法:");
    calculatorImpl.mult(x,y);
    System.out.println("x ! ：调用CalculatorImpl.fact()方法:");
    calculatorImpl.fact(x);
    System.out.println("x / y：调用CalculatorImpl.div()方法:");
    calculatorImpl.div(x,y);
    System.out.println("x % y：调用CalculatorImpl.mod()方法:");
    calculatorImpl.mod(x,y); 
    System.out.println("| x |：调用CalculatorImpl.abs()方法:");
    calculatorImpl.abs(x);   
    System.out.println("- x ：调用CalculatorImpl.oppo()方法:");
    calculatorImpl.oppo(x);
  }
}

class CalculatorImpl implements SubCalculatorInterface
{
  public int plus(int x,int y)
  {
    int t=x+y;
    System.out.println(x+" + "+y+" = "+t);
    return t;
  }
  public int mult(int x,int y)
  {
    int t=x*y;
    System.out.println(x+" * "+y+" = "+t);
    return t;
  }
  public int mod(int x,int y)
  {
    int t=x%y;
    System.out.println(x+" % "+y+" = "+t);
    return t;
  }
  public int div(int x,int y)
  {
    int t=x/y;
    System.out.println(x+" / "+y+" = "+t);
    return t;
  }
  public int abs(int x)
  {
    int t=x>=0 ? x:x*(-1);
    System.out.println("| "+x+" | = "+t);
    return t; 
  }
  public int fact(int x)
  {
    if(x<0) return -1;
    int i,t=1;
    for(i=1;i<=x;i++)
      t*=i;
    System.out.println(x+" ! = "+t);
    return t;
  }
  public int oppo(int x)
  {
     System.out.println("-( "+x+" ) = "+(-1)*x);
     return (-1)*x;
  }
}

interface CalculatorInterface
{
  int plus(int x,int y);
  int mult(int x,int y);
  int oppo(int x);
  int abs(int x);
}

interface SubCalculatorInterface extends CalculatorInterface
{
  int div(int x,int y);
  int mod(int x,int y);
  int fact(int x);
}
//接口不能使用static定义方法，缺省的是public，abstract！
//实现接口的类的方法应该是public！
