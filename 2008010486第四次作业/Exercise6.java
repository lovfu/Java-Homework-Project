import java.io.*;
class Exercise6
{
	public static void main(String args[])
	{
		int  i,n;
		try
		{
			System.out.println("请输入信息所在的文件名：");
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			String str=in.readLine();
			File file=new File(System.getProperty("user.dir"),str);
			BufferedReader input=new BufferedReader(new FileReader(file));
			input.mark(10000);
			i=0;
			while((str=input.readLine())!=null)
				i++;
			n=i;
			input.reset();
			String sName[]=new String[n];
			String sNum[]=new String[n];
			String sTemp[]=new String[2];
			i=0;
			while(((str=input.readLine())!=null))
			{
				sTemp=str.split(" ");
				sName[i]=new String(sTemp[0]);
				sNum[i]=new String(sTemp[1]);
				i++;
			}
			System.out.print("文件"+file.getName()+"中存储的姓名有: ");
			for(i=0;i<n;i++)
				System.out.print(sName[i]+" ");
			System.out.println();
			System.out.print("文件"+file.getName()+"中存储的学号有: ");
			for(i=0;i<n;i++)
				System.out.print(sNum[i]+" ");
			System.out.println();
		}
		catch(IOException e)
		{
			System.out.println("程序出现IO异常！");
		}
	}
}

