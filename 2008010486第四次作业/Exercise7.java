import java.io.*;
class Exercise7
{
	public static void main(String args[])
	{
		File file=new File(System.getProperty("user.dir"));
		String strName[]=file.list();
		int i=0;
		for(i=0;i<strName.length;i++)
		{
			if(isMatch(strName[i]))
				System.out.println(strName[i]);
		}
	}
	static boolean isMatch(String str)
	{
		return (str.regionMatches(str.length()-4,".txt",0,4)||str.regionMatches(str.length()-4,".doc",0,4));
	}
}
