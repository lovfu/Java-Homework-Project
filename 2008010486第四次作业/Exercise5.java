import java.io.*;
class Exercise5
{
	public static void main(String args[])
	{
		try
		{
			System.out.println("请输入存储的文件名：");
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			String str=in.readLine();
			File file=new File(System.getProperty("user.dir"),str);
			if(file.createNewFile())
				System.out.println(str+"文件创建成功！");
			else
				System.out.println(str+"文件已存在,格式化原文件...");
			PrintStream output=new PrintStream(file);
			BufferedWriter out=new BufferedWriter(new OutputStreamWriter(System.out));
			while(true)
			{
				System.out.println("请输入姓名和学号信息，姓名和学号中间用空格隔开：");
				System.out.println("如若退出，请输入quit或QUIT：");
				str=in.readLine();
				if(str.equals("quit")||str.equals("QUIT"))
					break;
				output.println(str);
			}
			output.close();	
		}
		catch(IOException e)
		{
			System.out.println("程序出现IO异常！");
		}
	}
}
