import java.io.File;
class Exercise4
{
	public static void main(String args[])
	{
		num=-1;
		File file=new File(System.getProperty("user.dir"));
		run(file);
	}
	static int num;//空格的个数
	static void run(File file) //递归函数
	{
		num++;//深入一层，则多一个空格。
		FilesList filesList=new FilesList(file);
		int i=0;
		int j=0;
		while(i<filesList.childFilesURL.length)
		{
			if(filesList.childFilesURL[i].isDirectory())
			{
				for(j=0;j<num;j++)
					System.out.print("  ");
				System.out.println("<"+filesList.childFilesName[i]+">");
				run(filesList.childFilesURL[i]);
			}
			else
			{
				for(j=0;j<num;j++)
				System.out.print("  ");
					System.out.println(filesList.childFilesName[i]);
			}
			i++;
		}
		num--;//跳出该层，则少一个空格。
	}
}

class FilesList
{
	File thisFolderURL;
	File[] childFilesURL;
	String[] childFilesName;
	FilesList(File f)
	{
		thisFolderURL=f;
		childFilesURL=thisFolderURL.listFiles();
		childFilesName=thisFolderURL.list();
	}
}
//注意：使用length判断数组的大小。
