class Exercise1
{
	
	public static void main(String args[])
	{
		int i;
		TestThread thread[] = new TestThread[4];
		for(i = 0;i < 4; i++)
		{
			thread[i] = new TestThread(i);
			if(i == 0)
				thread[i].setPriority(10);
			if(i == 1)
				thread[i].setPriority(1);
			thread[i].start();
		}
	}
}

class TestThread extends Thread
{
	public TestThread(int i)
	{
		super("Thread "+i);
	}
	public void run()
	{
		for(int i = 1;i <= 30; i++)
			System.out.println(getName()+" : "+i);
	}
}
