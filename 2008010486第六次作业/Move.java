import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class Move implements ActionListener
{
	JFrame frame;
	JButton button;
	int a,b;
	public static void main(String args[])
	{
		new Move().go();
	}
	public void go()
	{
		a = 0;
		b = 0;
		frame = new JFrame("Move");
		button = new JButton();
		frame.getContentPane().setBackground(Color.green);
		button.setBackground(Color.red);

		Timer time = new Timer(30,this);//其本身就是一个listener
		frame.setVisible(true);
		frame.setLayout(null);
		frame.setSize(300,330);
		frame.validate();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().add(button);
		button.addActionListener(this);//其本身就是一个listener

		button.setBounds(0,0,30,30);
		time.start();
	}
	public void actionPerformed(ActionEvent e)
	{
		if(a == 0 && b < frame.getWidth() - 30)
			b = b + 10;
		else if(a <frame.getHeight() - 60 && b == frame.getWidth() - 30)
			a = a + 10;
		else if(a == frame.getHeight() - 60 && b > 0)
			b = b - 10;
		else if(a > 0 && b == 0)
			a = a - 10;
		button.setBounds(b,a,30,30);
	}
}
