import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*; 
import java.util.*;
import java.applet.*;

public class AudioApplet extends Applet implements MouseListener,ListSelectionListener 
{
	JButton play;
	JButton stop;
	JButton loop;
	JList musicList;
	String[] musicName;
	AudioClip audio;
	int total;
	
	public void init()
	{
		int i;
		String str;
		total = Integer.parseInt(getParameter("Total"));
		musicName = new String[total];
		for(i = 0;i < total;i++)
		{
			musicName[i] = new String();
			musicName[i] = getParameter("MusicName"+i);
		}
			
		audio=getAudioClip(getDocumentBase(),musicName[10]);			
		play = new JButton("Play");
		stop = new JButton("Stop");
		loop = new JButton("Loop");
			
		musicList = new JList(musicName);
		JScrollPane scrollPane = new JScrollPane(musicList);
		
		add(play);
		add(stop);
		add(loop);
		add(musicList);
		add(scrollPane);
		play.addMouseListener(this);
		stop.addMouseListener(this);
		loop.addMouseListener(this);
		musicList.addListSelectionListener(this);
		
		setLayout(null);
		musicList.setBounds(0,0,300,280);
		play.setBounds(0,280,100,70);
		stop.setBounds(100,280,100,70);
		loop.setBounds(200,280,100,70);
		musicList.setSelectedIndex(0);
	}
	public void mouseClicked(MouseEvent e)
	{
		if(e.getComponent() == play)
		{
			audio.play();
			return;
		}
		if(e.getComponent() == stop)
		{
			audio.stop();
			return;
		}
		if(e.getComponent() == loop)
		{
			audio.loop();
			return;
		}
		
	}
	public void valueChanged(ListSelectionEvent e)
	{
		audio.stop();
		audio=getAudioClip(getDocumentBase(),musicName[musicList.getSelectedIndex()]);
	}
	public void mouseExited(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mousePressed(MouseEvent e)  {}
}
