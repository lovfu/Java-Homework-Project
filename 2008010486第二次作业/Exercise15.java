class Exercise15
{
  public static void main(String args[])
  {
    int matrixA[][]={{2,3,4},{7,8,9}};
    int matrixB[][]={{5,7,9,11},{6,8,19,14},{8,4,9,3}};
    int matrixC[][]={{23,45},{78,22},{88,99},{43,66}};
    int matrixT[][]=new int[2][4];
    int matrixR[][]=new int[2][2];
    int i,j,k;
    for(i=0;i<2;i++)
    {
      for(j=0;j<4;j++)
      {
        matrixT[i][j]=0;
        for(k=0;k<3;k++)
          matrixT[i][j]+=matrixA[i][k]*matrixB[k][j];
      }
    }
    for(i=0;i<2;i++)
    {
      for(j=0;j<2;j++)
      {
        matrixR[i][j]=0;
        for(k=0;k<4;k++)
          matrixR[i][j]+=matrixT[i][k]*matrixC[k][j];
      }
    }
    for(i=0;i<2;i++)
    {
      for(j=0;j<2;j++)
      {
        System.out.print(matrixR[i][j]+" ");
      }
      System.out.println();
    }
  }
}