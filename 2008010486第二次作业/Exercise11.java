class Exercise11
{
  public static void main(String args[])
  {
    int i;
    for(i=6;i<=100;i=i+2)
    {
      Num n=new Num(i);
      if(n.isPrimeComposed()==false)
      {
        System.out.println("哥德巴赫猜想是错误的！");  
      }
      n.showPrime();
    }
    System.out.println("哥德巴赫猜想是正确的！");  
  }
}

class Num
{
  private int num;
  Num(int i)
  {
    num=i;
  } //构造函数
  private int num1;//素数1
  private int num2;//素数2
  
  private boolean isPrime(int n)
  {
    int i;
    for(i=2;i<=n/2;i++)
    {
      if(n%i==0)
      {
        return false;
      }
    }
    return true;
  } //判断是否为素数的函数，private
  
  public boolean isPrimeComposed()
  {
    int i;
    int tNum;
    for(i=3;i<=num/2;i=i+2)
    {
      if(isPrime(i))
      {
        tNum=num-i;
        if(isPrime(tNum))
        {
          num1=i;
          num2=tNum;
          return true;
        }
      }
    }
    return false;
  }//判断成员变量num是否能够被两个素数之和构成的函数，public
  
  public void showPrime()
  {
     System.out.println(num+" = "+num1+" ＋ "+num2); 
  }//输出两个构成该数的素数
}