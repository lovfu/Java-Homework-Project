import java.util.regex.*;
class Exercise18
{
   public static void main(String args[])
   {
      StringBuffer str=new StringBuffer("解决DCOM的问题主要是解决程序配置和部署的问题。由于DCOM涉及到在多台计算机上运行的程序,所以潜在的问题比在单机上使用COM时要大。其他可能需要解决的问题包括程序和网络协议之间的安全机制。");
      StringBuffer strAppend=new StringBuffer("因为在默认情况下COM安全是打开的,所以只要试图访问COM对象的COM程序或客户程序启动COM对象,就会开始进行安全检查。");
      str=str.append(strAppend);
      Pattern p=Pattern.compile("[A-Z]");
      Matcher m=p.matcher(str);
      StringBuffer sb=new StringBuffer();
      while(m.find())
      {
        m.appendReplacement(sb,m.group().toLowerCase());
      }
      m.appendTail(sb);
      System.out.println(sb);
   }
}
