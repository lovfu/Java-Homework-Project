class CharList
{
  private char list[];
  private int length;
  private int tail;//队尾的地址
  private int head;//队头的地址
  
  CharList(int i)
  {
    length=i;
    tail=-1;//表示不存在元素，即不存在队尾
    head=-1;//表示不存在元素，即不存在队头
    list=new char[i];
  }
  
  public void put(char c)
  {
    if(head==tail+1||(tail==head+length-1))
    {
      System.out.println("队列满,无法 put!");
      return;
    }
    if(tail<length-1) tail++;
    else tail=0;
    if(head==-1)//考虑由不存在元素转变为存在元素之间的过程
      head=0;
    list[tail]=c;
  }
  
  public char get()
  {
    if(tail==-1)//不存在元素，即tail和head都为-1
    {
      System.out.println("队列空,无法 get!");
      return 0;
    }
    char c=list[head];
    if(tail==head)
    {
      tail=-1;
      head=-1;
      return c;
    }//考虑即将过渡到不存在元素时的情形。
    if(head<length-1) head++;
    else head=0;
    return c;
  }
}

class CharListDemo
{
  public static void main(String args[])
  {
    CharList bigList=new CharList(100);
    CharList smallList=new CharList(4);
    char c;
    int i;
    for(c='a';c<='z';c++)
    {
      bigList.put(c);
    }
    for(i=0;i<26;i++)
    {
      System.out.println(bigList.get());
    } 
    smallList.put('Z');
    smallList.put('Y');
    smallList.put('X');
    smallList.put('W');
    smallList.put('V');
    for(i=0;i<5;i++)
    {
      System.out.println(smallList.get());
    }
  }
}

/*
注意方法中，类属性的状态转变过程，特别是非线性变化，比如从存在到不存在，从不存在到存在。
*/
