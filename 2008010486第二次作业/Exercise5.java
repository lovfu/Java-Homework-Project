class Exercise5
{
  public static void main(String args[])
  {
    int intResult=10/3;
    int intRemainder=10%3;
    double doubleResult=10.0/3;
    double doubleRemainder=10.0%3;
    System.out.println("intResult==doubleResult?");
    System.out.println(intResult==doubleResult);
    System.out.println("intRemainder==doubleRemainder?");
    System.out.println(intRemainder==doubleRemainder);
  }
}