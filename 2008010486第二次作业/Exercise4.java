class Exercise4
{
  public static void main(String args[])
  {
    Integer intObj;
    intObj=new Integer(123);
    int i=intObj;
    float f=intObj;
    double d=intObj;
    System.out.println("i="+i);
    System.out.println("f="+f);
    System.out.println("d="+d);
  }
}
//注意：public static void 三者的顺序不能调换，否则会出错。