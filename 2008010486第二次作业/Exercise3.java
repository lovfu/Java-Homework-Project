public class Exercise3 
{
  public static void main(String args[])
  {
    int i=10;
    short s=10;
    double d=10;
    System.out.println("当i、s、d都被赋值为10时");
    System.out.println("i="+i);
    System.out.println("s="+s);
    System.out.println("d="+d);
    i/=3;
    s/=3;
    d/=3;
    System.out.println("当i、s、d都被赋值为原值除以3时");
    System.out.println("i="+i);
    System.out.println("s="+s);
    System.out.println("d="+d);
  }
}
//注意：int i;在main外时，只是类的一个实例变量，而main是一个类方法，不能访问实例变量。因此，main函数访问的变量都需要在main内进行定义，或者访问类变量。
//注意：s=s/3；会损失精度，因此java编译器报错。修正后的方法应该为s/=3;
//注意：java的println方法，自动带有换行的功能。