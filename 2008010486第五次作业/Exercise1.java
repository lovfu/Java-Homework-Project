import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Exercise1 extends MouseAdapter
{
		private JFrame frame;
		private JTextArea text;
		private JRadioButton radio1;
		private JRadioButton radio2;
		private JCheckBox check1;
		private JCheckBox check2;
		private ButtonGroup buttonGroup;
		private JButton button;
		private JScrollPane scrollPane;
		
	public static  void main(String args[])
	{
		Exercise1 that=new Exercise1();
		that.go();
	}
	
	public void go()
	{
		frame = new JFrame("example");
		radio1 = new JRadioButton("blue");
		radio2 = new JRadioButton("red");
		buttonGroup = new ButtonGroup();		
		check1 = new JCheckBox("checkBox1");
		check2 = new JCheckBox("checkBox2");
		button = new JButton("OK");
		text = new JTextArea(5,20);
		JScrollPane scrollPane = new JScrollPane(text,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		frame.setLayout(null);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		radio1.addMouseListener(this);
		radio2.addMouseListener(this);
		button.addMouseListener(this);
		check1.addMouseListener(this);
		check2.addMouseListener(this);		
		
		buttonGroup.add(radio1);
		buttonGroup.add(radio2);
		frame.getContentPane().add(check1);
		frame.getContentPane().add(check2);
		frame.getContentPane().add(radio1);
		frame.getContentPane().add(radio2);
		frame.getContentPane().add(scrollPane);
		frame.getContentPane().add(button);
				
		check1.setBounds(50,10,100,50);
		check2.setBounds(50,40,100,50);
		radio1.setBounds(200,10,100,50);
		radio2.setBounds(200,40,100,50);
		scrollPane.setBounds(0,100,300,100);
		button.setBounds(00,200,300,50);
		frame.setSize(305,285);
		
		text.setForeground(Color.red);
	}
	
	public void mouseClicked(MouseEvent e)
	{
		if(e.getComponent()==button)
			text.append("button is beated\n");
		if(e.getComponent()==radio1)
			text.setForeground(Color.blue);
		if(e.getComponent()==radio2)
			text.setForeground(Color.red);
		if(e.getComponent()==check1)
			if(check1.isSelected()==true)
				text.append("checkBox1 is selected\n");
			else
				text.append("checkBox1 is not selected\n");
		if(e.getComponent()==check2)
			if(check2.isSelected()==true)	
				text.append("checkBox2 is selected\n");
			else
				text.append("checkBox2 is not selected\n");
	}
}
//注意：e.getComponent()对radioButton没有效果。
//ButtonGroup.setSelected对radioButton没有效果。
