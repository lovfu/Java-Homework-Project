import java.util.*;
public class PartMatrix
{
	private static int [][]id = new int[9][9];
	private static Random random;
	public static int [][]create()
	{
		int i,j,t;
		random = new Random();
		int tempH[] = new int[10];
		int tempV[] = new int[10];
		int tempP[] = new int[10];
		int a[][] = new int[9][9];
		for(i=0;i<9;i++)
		{
			while(true)
			{
				t=random.nextInt(10);
				if(t!=0&&tempH[t]==0)
				{
					tempH[t]=1;
					a[0][i]=t;
					if(i==0)
						tempV[t]=1;
					if(i==0||i==1||i==2)
						tempP[t]=1;
						break;
				}
			}
		}
		for(i=1;i<9;i++)
		{
			while(true)
			{
				t=random.nextInt(10);
				if(t!=0&&tempV[t]==0)
				{
					if((i==1||i==2)&&tempP[t]!=0)
						continue;
					tempV[t]=1;
					a[i][0]=t;
					break;
				}
			}
		}
		return a;
		//以上创造出了只有第一列和第一行的数独，以下解决这个数独，得到的第一解便是需要的随机Matrix。
	}
	public static int[][] getRandomMatrix(int l)
	{
		int i,j,x,y,t;
		int temp[][];
		id=create();
		temp = Matrix.answer(id);///重点！！！！！！！！！！！！！！！！！！！！！！！！！
		for(i=0;i<9;i++)
			for(j=0;j<9;j++)
				id[i][j]=temp[i][j];///重点！！！！！！！！！！！！！！！！！！！！！！！！！
		if(l>=9||l<1)
			return id;
		else
		{
			int h[][] = new int[3][3];
			for(i=0;i<3;i++)
			{
				for(j=0;j<3;j++)
				{
					while(h[i][j]<l)
					{
						t=random.nextInt(9);
						x=i*3+t/3;
						y=j*3+t%3;
						if(id[x][y]!=0)
						{
							id[x][y]=0;
							h[i][j]++;
						}
					}
				}
			}
			return id;
		}
	}
}
//注意：需要先对函数返回值进行引用，然后再进行赋值，否则会出现不可避免的错误。
