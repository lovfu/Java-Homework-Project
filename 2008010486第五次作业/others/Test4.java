import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
public class Test4 extends JFrame implements MenuListener,ActionListener{
	JTextArea output;
	JScrollPane scrollPane;
	JMenuBar menuBar;
	JMenu menu, submenu,abcmenu;
	JMenuItem menuItem, menuItemExit;
	JCheckBoxMenuItem cbMenuItem;
	JRadioButtonMenuItem blueMenuItem,redMenuItem;
	public Test4() {
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	Container contentPane = getContentPane();
	output = new JTextArea(5, 30);
	//output.setEditable(false);
	scrollPane = new JScrollPane(output);
	contentPane.add(scrollPane, BorderLayout.CENTER);
	menuBar = new JMenuBar();
	setJMenuBar(menuBar);
	menu = new JMenu("manage");
	menuBar.add(menu);
	//�ڶ����˵�
	submenu = new JMenu("color");
	redMenuItem = new JRadioButtonMenuItem("Red  Ctrl+Shift+R");
	submenu.add(redMenuItem);
	blueMenuItem = new JRadioButtonMenuItem("Blue  Ctrl+Shift+E");
	submenu.add(blueMenuItem);
	menu.add(submenu);
	menu.addSeparator();
	
	ButtonGroup colorGroup = new ButtonGroup();
	colorGroup.add(blueMenuItem);
	blueMenuItem.addActionListener(this);
	colorGroup.add(redMenuItem);
	redMenuItem.addActionListener(this);
	
	cbMenuItem = new JCheckBoxMenuItem("check");
	menu.add(cbMenuItem);
	
	menuItemExit = new JMenuItem("Exit  Ctrl+Shift+E");
	menuItemExit.addActionListener((ActionListener)this);
	menuItemExit.setMnemonic(KeyEvent.VK_C);
	menu.add(menuItemExit);
	
	abcmenu = new JMenu("help");
	abcmenu.addMenuListener((MenuListener)this);
	menuBar.add(abcmenu);
	}
	public static void main(String[] args) {
			Test4 window = new Test4();
			window.setTitle("menuexample");
			window.setSize(450, 260);
			window.setVisible(true);
	}
	
	public void menuSelected(MenuEvent e){
		Object source = e.getSource();
		if (source == abcmenu){
			output.append("\n this is homework5, our java homework is too much!!!");
		}
	}
	public void menuCanceled(MenuEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	//@Override
	public void menuDeselected(MenuEvent arg0) {
		// TODO Auto-generated method 	stub
		
	}
	public void actionPerformed(ActionEvent e){
		Object source = e.getSource();
		if (source == menuItemExit){
			System.exit(0);
		}else if (source == redMenuItem){
			if (redMenuItem.isSelected()){
				output.setForeground(Color.RED);
			}
		}else if (source == blueMenuItem){
			if (blueMenuItem.isSelected()){
				output.setForeground(Color.BLUE);
			}
		}
		
	}
	/*public void keyPressed(KeyEvent e){
		Object source = e.getSource();
		if (source == VK_C){
			
		}
	}*/
}