import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class test implements ActionListener, ItemListener {
		private static JButton button = new JButton("OK");
		private static JTextArea text = new JTextArea();
		private static JScrollPane jp = new JScrollPane(text);
		
		
		private static JCheckBox box1 = new JCheckBox("checkBox1");
		private static JCheckBox box2 = new JCheckBox("checkBox2");
		private static JRadioButton blue = new JRadioButton("blue");
		private static JRadioButton red = new JRadioButton("red");
		
		public static void main(String args[]){
			test xsz = new test();
			xsz.go();
		}
		private void go(){
			JFrame myFrame = new JFrame("example");
			myFrame.setLayout(new BorderLayout());
			
			JPanel p1 = new JPanel();
			JPanel p2 = new JPanel();
			
			p1.setLayout(new GridLayout(2,2));
			
			box1.addItemListener(this);
			ButtonGroup colorGroup = new ButtonGroup();
			colorGroup.add(blue);
			blue.addActionListener(this);
			colorGroup.add(red);
			red.addActionListener(this);
			box2.addItemListener(this);
			button.addActionListener(this);
			jp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
			jp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); 
			
			p1.add(box1);
			p1.add(blue);
			p1.add(box2);
			p1.add(red);
			
			text.setForeground(Color.BLUE);
			myFrame.getContentPane().add("North",p1);
			myFrame.getContentPane().add("Center",jp);
			myFrame.getContentPane().add("South",button);
			
			myFrame.setSize(500,500);
			myFrame.setVisible(true);
		
		}
		
		public  void actionPerformed(ActionEvent e){
				if (e.getSource() == red){
					if (red.isSelected()){
						text.setForeground(Color.RED);
					}
				}
				if (e.getSource() == blue){
					if (blue.isSelected()){
						text.setForeground(Color.BLUE);
					}
				}
				if (e.getSource() == button){
					text.append("\n button is beated");
				}
		}
		
		public void itemStateChanged(ItemEvent e){
			Object source = e.getItemSelectable();
			if (source == box1){
				if (box1.isSelected()){
					text.append("\ncheckBox1 is selected");
					
				}
			}
			if (source == box2){
				if (box2.isSelected()){
					text.append("\ncheckBox2 is selected");
				}
			}
		}
}