import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.awt.*;
public class Test3 implements ActionListener{
	JFrame f;
	JTextArea ta;
	JFileChooser fc;
	Container c;
	File myFile;
public static void main(String args[]){
	Test3 demo=new Test3();
	demo.go();
}
void go(){
	JFrame f=new JFrame("File Chooser Demo");
	f.setLayout(new GridLayout(2,1,50,50));
	JButton b=new JButton("Open file");
	JButton b2 = new JButton("save file");
	JPanel panel= new JPanel();
	panel.setLayout(new GridLayout(1,2,50,50)); 
	
	
	ta=new JTextArea("Where is your file path?",10,30);
	b.addActionListener(this);
	c=f.getContentPane();
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	panel.add(b);
	panel.add(b2);
	f.getContentPane().add("Center",panel);
	f.getContentPane().add("South",ta);
	f.setSize(300,300);
	f.setVisible(true);
}
public void actionPerformed(ActionEvent e){
	fc=new JFileChooser();
	int selected=fc.showOpenDialog(c);
	if (selected==JFileChooser.APPROVE_OPTION){
		myFile=fc.getSelectedFile();
		ta.setText("You have selected file: "+myFile.getAbsolutePath());
	}
}
}