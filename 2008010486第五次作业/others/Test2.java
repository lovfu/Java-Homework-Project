import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Test2 implements ActionListener{
	JButton dialogButton = new JButton("dialog");
	JButton ok = new JButton("ok");
	JTextArea text = new JTextArea();
	JScrollPane jp = new JScrollPane(text);
	JTextField textareaName = new JTextField("");
	JTextField textareaPassword = new JTextField("");
	JLabel name = new JLabel("name");
	JLabel password = new JLabel("password");
	JFrame frame1 = new JFrame("dialog");
	
	public void go(){
		//JFrame frame1 = new JFrame("dialog");
		frame1.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame1.setLayout(new BorderLayout());
		dialogButton.addActionListener(this);
		
		frame1.getContentPane().add("North", dialogButton);
		frame1.getContentPane().add("Center", jp);
		frame1.setSize(200, 200);
		frame1.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e){
		Object source = e.getSource();
		if(source == dialogButton){
			//if(dialogButton.isSelected()){
				JFrame frame2 = new JFrame("dialog");
				frame2.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				});
				frame2.setLayout(new GridLayout(0,1,40,50));
				
				JPanel panel1 = new JPanel();
				panel1.setLayout(new GridLayout(1,2));
				panel1.add(name);
				panel1.add(textareaName);
				
				JPanel panel2 = new JPanel();
				panel2.setLayout(new GridLayout(1,2));
				panel2.add(password);
				panel2.add(textareaPassword);
				ok.addActionListener(this);
				frame2.getContentPane().add(panel1);
				frame2.getContentPane().add( panel2);
				frame2.getContentPane().add( ok);
				frame2.setSize(200, 200);
				frame2.setVisible(true);
			//}
		}
		if(source == ok){
			text.append("name:"+textareaName.getText()+"\n\r");
			text.append("password:"+textareaPassword.getText()+"\n\r");
		}
	}
	
	public static void main(String args[]){
		Test2 example = new Test2();
		example.go();
	}
}
