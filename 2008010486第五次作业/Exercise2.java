import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Exercise2 extends MouseAdapter
{
	private JFrame frame;
	private JTextArea text;
	private JButton button;
	private JScrollPane scrollPane;
	private JButton buttonOK;
	private JTextArea text1;
	private JTextArea text2;
	private JDialog dialog ;
	public static  void main(String args[])
	{
		Exercise2 that=new Exercise2();
		that.go();
	}

	public void go()
	{
		frame = new JFrame("Dialog");	
		button = new JButton("Dialog");
		text = new JTextArea(20,20);
		JScrollPane scrollPane = new JScrollPane(text,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

		frame.setLayout(null);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);

		button.addMouseListener(this);

		frame.getContentPane().add(scrollPane);
		frame.getContentPane().add(button);

		button.setBounds(00,0,300,30);
		scrollPane.setBounds(0,30,300,250);
		frame.setSize(305,315);
	}

	public void mouseClicked(MouseEvent e)
	{
		if(e.getComponent()==button)
		{
			dialog = new JDialog(frame,"Dialog");
			text1= new JTextArea(1,10);
			text2= new JTextArea(1,10);
			buttonOK= new JButton("OK");
			JLabel name = new JLabel("Name");
			JLabel passWord = new JLabel("PassWord");

			dialog.setVisible(true);
			dialog.setLayout(null);
			
			buttonOK.addMouseListener(this);

			name.setBounds(5,0,100,30);
			passWord.setBounds(5,30,100,30);
			text1.setBounds(100,0,150,25);
			text2.setBounds(100,35,150,25);
			buttonOK.setBounds(0,60,250,40);
			dialog.setSize(250,130);
			
			dialog.getContentPane().add(name);
			dialog.getContentPane().add(passWord);
			dialog.getContentPane().add(text1);
			dialog.getContentPane().add(text2);
			dialog.getContentPane().add(buttonOK);			
		}
		else if(e.getComponent()==buttonOK)
		{
			String str1 = "Name: " + text1.getText();
			String str2 = (new String("PassWord: ")) + text2.getText();
			text.append(str1 + "\n" + str2+"\n");
			dialog.setVisible(false);	
		}
	}
}
