import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class Exercise5 extends MouseAdapter
{
	private JFrame frame;
	private JTextArea text;
	private JScrollPane scrollPane;
	static int n;
	public static void main(String args[])
	{
		n=0;
		Exercise5 that = new Exercise5();
		that.go();
	}

	public void go()
	{
		frame = new JFrame("Mouse Event");
		text = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(text,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

		frame.setLayout(null);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		frame.getContentPane().add(scrollPane);
		frame.setSize(300,300);
		scrollPane.setBounds(0,0,295,265);
		text.setEditable(false);

		text.addMouseListener(this);
	}
	public void  mouseClicked(MouseEvent e)
	{
		if(e.getButton()==e.BUTTON1)
		{
			n++;
			text.setText("点击左键\n点击位置：X = "+e.getX()+"  Y = "+e.getY()+"\n点击次数： "+n);
		}
		if(e.getButton()==e.BUTTON2)
		{
			n++;
		}
		if(e.getButton()==e.BUTTON3)
		{
			n++;
		}		
	}
}

