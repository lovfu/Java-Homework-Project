import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
class Exercise3 extends MouseAdapter
{
	private JFrame frame;
	private JScrollPane scrollPane;
	private JButton button1;
	private JButton button2;
	private JTextArea text;
	private JFileChooser fileChooser;
	
	public static void main(String args[])
	{
		Exercise3 that = new Exercise3();
		that.go();
	}

	public void go()
	{
		frame = new JFrame("File Dialog");
		text = new JTextArea(6,20);
		scrollPane = new JScrollPane(text,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		button1 = new JButton("Open File");
		button2 = new JButton("Savd File");

		button1.addMouseListener(this);
		
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setLayout(null);

		button1.setBounds(20,10,120,30);
		button2.setBounds(160,10,120,30);
		scrollPane.setBounds(0,50,295,165);
		frame.setSize(300,250);
		frame.getContentPane().add(button1);
		frame.getContentPane().add(button2);
		frame.getContentPane().add(scrollPane);
		frame.setVisible(true);
	}
	
	public void mouseClicked(MouseEvent e)
	{
		fileChooser = new JFileChooser();
		fileChooser.showOpenDialog(frame);
		File file = fileChooser.getSelectedFile();
		text.append(file.toString()+"\n");
	}
}
