import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class Exercise4 extends MouseAdapter implements ActionListener
{
	private JFrame frame;
	private JMenuBar menuBar;
	private JMenu manage;
	private JMenu help;
	private JMenu color;
	private JSeparator separator;
	private JCheckBoxMenuItem check;
	private JMenuItem exit;
	private JPopupMenu managePopupMenu;
	private JPopupMenu colorPopupMenu;
	private JMenuItem red;
	private JMenuItem blue;
	private JTextArea text;
	private JScrollPane scrollPane;

	public static void main(String args[])
	{
		Exercise4 that = new Exercise4();
		that.go();
	}

	public void go()
	{
		frame = new JFrame("Menu Example");
		menuBar = new JMenuBar();
		manage = new JMenu("Manage");
		help = new JMenu("Help");
		color = new JMenu("Color");
		separator = new JSeparator();
		check = new JCheckBoxMenuItem("Check");
		exit = new JMenuItem("Exit",KeyEvent.VK_E);
		managePopupMenu = new JPopupMenu();
		colorPopupMenu = new JPopupMenu();
		red = new JMenuItem("Red",KeyEvent.VK_R);
		blue = new JMenuItem("Blue",KeyEvent.VK_B);
		text = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(text,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

		frame.getContentPane().add(menuBar);
		frame.getContentPane().add(scrollPane);
		menuBar.add(manage);
		menuBar.add(help);
		manage.add(managePopupMenu);
		managePopupMenu.add(color);
		managePopupMenu.add(separator);
		managePopupMenu.add(check);
		managePopupMenu.add(exit);
		color.add(colorPopupMenu);
		colorPopupMenu.add(red);
		colorPopupMenu.add(blue);
		
		frame.addMouseListener(this);
		manage.addMouseListener(this);
		color.addMouseListener(this);
		exit.addMouseListener(this);
		red.addMouseListener(this);
		blue.addMouseListener(this);
		help.addMouseListener(this);
		text.addMouseListener(this);
		menuBar.addMouseListener(this);
		exit.addActionListener(this);
		red.addActionListener(this);
		blue.addActionListener(this);
		
		exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,ActionEvent.CTRL_MASK+ActionEvent.SHIFT_MASK));
		red.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,ActionEvent.CTRL_MASK+ActionEvent.SHIFT_MASK));
		blue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,ActionEvent.CTRL_MASK+ActionEvent.SHIFT_MASK));

		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setVisible(true);
		frame.setSize(500,500);
		menuBar.setBounds(0,0,500,30);
		scrollPane.setBounds(0,30,495,436);
		text.setForeground(Color.blue);
		
		
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==exit)
		{
			System.exit(0);
		}
		if(e.getSource()==red)
		{
			text.setForeground(Color.red);
			red.setSelected(true);
		}
		if(e.getSource()==blue)
		{
			text.setForeground(Color.blue);
			blue.setSelected(true);
		}
	}
	public void mouseClicked(MouseEvent e)
	{
		if(e.getComponent()==manage)
		{
			if(managePopupMenu.isVisible()==true)
			{
				managePopupMenu.setVisible(false);
				colorPopupMenu.setVisible(false);
			}
			else
			{
				managePopupMenu.show(e.getComponent(),0,30);
				managePopupMenu.setVisible(true);					
			}
		}
		if(e.getComponent()==text)
		{
			managePopupMenu.setVisible(false);
			colorPopupMenu.setVisible(false);
		}			
		if(e.getComponent()==color)
		{
			if(colorPopupMenu.isVisible()==true)
			{
				colorPopupMenu.setVisible(false);
			}
			else
			{
				colorPopupMenu.show(e.getComponent(),142,0);
				colorPopupMenu.setVisible(true);
			}
		}

		if(e.getComponent()==help)
		{
			managePopupMenu.setVisible(false);
			colorPopupMenu.setVisible(false);
			text.setText("Copyright 2010-3000\nWang Hongwei\n2008010486\nCS 84\nAll rights reserved.");
			help.setSelected(false);
		}
		if(e.getComponent()==exit)
		{
			System.exit(0);
		}
		if(e.getComponent()==red)
		{
			text.setForeground(Color.red);
			managePopupMenu.setVisible(false);
			colorPopupMenu.setVisible(false);
		}
		if(e.getComponent()==blue)
		{
			text.setForeground(Color.blue);
			managePopupMenu.setVisible(false);
			colorPopupMenu.setVisible(false);
		}
		if(e.getComponent()==menuBar)
		{
			managePopupMenu.setVisible(false);
			colorPopupMenu.setVisible(false);
		}
	}
}
//鼠标：MouseListener
//键盘：AciontListener actionPerformed exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,ActionEvent.CTRL_MASK)
